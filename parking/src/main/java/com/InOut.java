package com;

public class InOut {
    private String in;
    private int time;
    private String out;

    public void setIn(int time,String day){
        this.time = time;
        this.in = day;
    }

    public String getIn(){
        return this.in;
    }

    public int getTime(){
        return this.time;
    }

    public void setOut(String checkOut){
        this.out = checkOut;
    }

    public String getOut(){
        return this.out;
    }
}