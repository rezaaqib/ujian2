package com;

import java.time.LocalDateTime;
import java.util.*;

public class Test {
    public void showParking() {
        Scanner inputUser = new Scanner(System.in);

        // InOut iOParkir = new InOut();
        // iOParkir.setIn(12,"am");
        // boolean timer = true;
        // System.out.println(iOParkir.getTime() + iOParkir.getIn());
        // LocalDateTime parkir = LocalDateTime.now();

        System.out.println("\n\n=====  Parking Smart System  =====\n");
        System.out.println("1.\tMotor");
        System.out.println("2.\tMobil");
        System.out.print("\nPilih Jenis Kendaraan Anda : ");
        String hasilInput = inputUser.next();

        switch (hasilInput) {
            case "1":
                double pMotor = 3;
                System.out.println("\nHarga Parkir 1 jam pertama : Rp. " + pMotor + "K\n");

                Scanner inputMasuk = new Scanner(System.in);
                System.out.print("Anda telah parkir selama (jam): ");
                double masuk = inputMasuk.nextDouble();
                // kondisi jam naik harga naik.
                if (masuk > 1) {
                    System.out.println("\nBiaya Parkir : Rp. " + (pMotor + 1 * masuk) + "K");
                } else {
                    System.err.println("ERROR");
                }
                break;
            case "2":
                double pMobil = 5;
                Scanner inputInMobil = new Scanner(System.in);
                System.out.println("\nHarga Parkir 1 jam pertama : Rp. " + pMobil + "K\n");

                System.out.print("\nAnda telah parkir selama (jam): ");
                double inMobil = inputInMobil.nextDouble();
                // kondisi jam naik harga naik.
                if (inMobil > 1) {
                    System.out.println("\nBiaya Parkir : Rp. " + (pMobil + 2 * inMobil) + "K");
                } else {
                    System.err.println("ERROR");
                }
                break;
            default:
                System.err.println("Input yang anda masukan bukan 1 / 2");
                break;
        }
        inputUser.close();
    }
}
